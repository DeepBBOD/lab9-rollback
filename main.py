from typing import List

import psycopg2
from psycopg2.extras import execute_values

con = psycopg2.connect(
    database="company", user="egorbak", password="", host="127.0.0.1", port="5432"
)
con.autocommit = False


def employee_insert():
    cur = con.cursor()

    try:
        print("Inserting one row....")
        cur.execute("INSERT INTO Employees VALUES (108, 20, 'Jane', 'Eyre')")
        con.commit()
    except psycopg2.DatabaseError as e:
        con.rollback()

    print("Inserting one row....")
    cur.execute("INSERT INTO Employees VALUES (109, 20, 'David', 'Rochester')")
    con.rollback()

    print("Inserted Data:")
    cur.execute("SELECT * from Employees")
    rows = cur.fetchall()
    print("\n".join([str(i) for i in rows]))


def insert_salaries(ids: List[int], employee_ids: List[int], amount: List[int]) -> bool:
    to_insert = []
    insert_query = "INSERT INTO Salary (id, employee_id, amount) values %s"
    cur = con.cursor()

    for i in range(min(len(ids), len(employee_ids), len(amount))):
        print(f"Inserting {i} row....")
        cur.execute(
            f"SELECT id FROM Salary WHERE employee_id={employee_ids[i]} or id={ids[i]} LIMIT 1;"
        )
        if cur.fetchone():
            print(f"Skipping {i}: employee_id or id already exist")
            continue
        to_insert.append((ids[i], employee_ids[i], amount[i]))

    try:
        execute_values(cur, insert_query, to_insert, template=None, page_size=100)
        print("Commiting data here....")
        con.commit()
    except BaseException as e:
        print(f"Error: {e}. Rollback")
        con.rollback()

    print("Inserted Data:")
    cur.execute("SELECT * from Salary")
    rows = cur.fetchall()
    print("\n".join([str(i) for i in rows]))


if __name__ == "__main__":
    # employee_insert()
    insert_salaries([1, 3], [2, 3], [3, "d"])
